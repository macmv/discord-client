package main

import (
  "fmt"
  "sync"
  "math"

  "gopkg.in/hraban/opus.v2"
  "github.com/xthexder/go-jack"
  "github.com/bwmarrin/discordgo"

  "gitlab.com/macmv/cbuf"
)

// JACK audio client. Will never be nil.
var client *jack.Client

// JACK audio ports. Used to read/write audio
// when in a voice channel.
var audio_input_l *jack.Port
var audio_input_r *jack.Port
var audio_output_l *jack.Port
var audio_output_r *jack.Port

// Current active voice connection. Will
// be nil if not connected to voice.
var connected_voice *discordgo.VoiceConnection

// OPUS audio. Used to encode/decode audio
// which is used by the discord udp connection.
var encoder *opus.Encoder
var decoder *opus.Decoder

type audio_buffer struct {
  // A list of []int16, which is pcm data.
  buf *cbuf.CyclicBuffer
  // This is a counter, which is reset every
  // time we skip audio.
  last_skip int
  speed float64
  // The user id for this audio buffer.
  user_id string
}

// This is a map of all incoming audio streams.
// It is updated by a separate goroutine, which
// makes the jack audio processing much simpler.
var audio_streams = make(map[uint32]*audio_buffer)
var audio_stream_locks = make(map[uint32]*sync.Mutex)

// Audio buffer for microphone. Opus packets and
// JACK frames are different sizes, so we need to
// buffer the audio received from JACK
var output_buffer []int16

func init() {
  create_jack_client()

  sample_rate := int(client.GetSampleRate())

  var err error
  encoder, err = opus.NewEncoder(sample_rate, 2, opus.AppVoIP)
  if err != nil {
    fmt.Println(err)
    return
  }

  decoder, err = opus.NewDecoder(sample_rate, 2)
  if err != nil {
    fmt.Println(err)
    return
  }
}

func create_jack_client() {
  var status int
  client, status = jack.ClientOpen("discord", jack.NoStartServer)
  if status != 0 {
    fmt.Println("Failed to open jack port, status: ", jack.StrError(status).Error())
  }
  code := client.SetProcessCallback(audio_process)
  if code != 0 {
    fmt.Println("Failed to set process callback: ", jack.StrError(code))
  }
  client.OnShutdown(func() {
    fmt.Println("Shutting down JACK client")
    create_jack_client()
  })
  code = client.Activate()
  if code != 0 {
    fmt.Println("Failed to activate client: ", jack.StrError(code))
    return
  }
  audio_input_l = client.PortRegister("input_l", jack.DEFAULT_AUDIO_TYPE, jack.PortIsInput, 0)
  audio_input_r = client.PortRegister("input_r", jack.DEFAULT_AUDIO_TYPE, jack.PortIsInput, 0)
  audio_output_l = client.PortRegister("output_l", jack.DEFAULT_AUDIO_TYPE, jack.PortIsOutput, 0)
  audio_output_r = client.PortRegister("output_r", jack.DEFAULT_AUDIO_TYPE, jack.PortIsOutput, 0)

  connect_defaults()
}

func connect_defaults() {
  // Speakers are audio inputs, as they want audio from JACK clients.
  outputs := client.GetPorts("", "", jack.PortIsPhysical|jack.PortIsInput)
  if len(outputs) == 0 {
    LOG.Println("Failed to connect to a physical output, as there is none")
    return
  }
  // We just use the first two physical ports. Not sure if there is a
  // better way to do this.
  client.Connect(audio_output_l.GetName(), outputs[0])
  client.Connect(audio_output_r.GetName(), outputs[1])

  // Microphones are audio outputs, as they output to JACK.
  inputs := client.GetPorts("", "", jack.PortIsPhysical|jack.PortIsOutput)
  if len(inputs) == 0 {
    LOG.Println("Failed to connect to a physical input, as there is none")
    return
  }
  // Again, just use the first one.
  client.Connect(inputs[0], audio_input_l.GetName())
  client.Connect(inputs[1], audio_input_r.GetName())
}

func audio_process(nframes uint32) int {
  if connected_voice == nil {
    return 0
  }
  // Audio receiving
  out_l := audio_output_l.GetBuffer(nframes)
  out_r := audio_output_r.GetBuffer(nframes)
  // Need to make sure we start with 0
  for i := range out_l { out_l[i] = 0 }
  for i := range out_r { out_r[i] = 0 }
  // Get audio from every other user in the channel
  for ssrc, buf := range audio_streams {
    audio_stream_locks[ssrc].Lock()
    buffer := buf.buf
    // buffer.Len() is in bytes, so we need to divide it by 2 for int16s
    speed_multiplier := float64(buffer.Len() / 2) - (float64(nframes) * 2 * 8)
    if buf.speed == 1 {
      if buf.last_skip > 50 && speed_multiplier > 1 {
        buf.speed = speed_multiplier
      }
    } else {
      if speed_multiplier < 0.5 {
        buf.speed = 1
      }
    }
    if buf.speed != 1 {
      buf.last_skip = 0
    }
    // remaining is the number of int16s
    remaining := int(float64(len(out_l) * 2) * buf.speed)
    pcm := buffer.PopInt16(remaining)
    add_pcm(buf.speed, out_l, out_r, pcm)
    buf.last_skip++
    audio_stream_locks[ssrc].Unlock()
  }
  // Audio sending
  in_l := audio_input_l.GetBuffer(nframes)
  in_r := audio_input_r.GetBuffer(nframes)
  pcm := make([]int16, nframes * 2)
  for i := range in_l {
    pcm[i * 2 + 0] = int16(in_l[i] * 32768)
    pcm[i * 2 + 1] = int16(in_r[i] * 32768)
  }
  output_buffer = append(output_buffer, pcm...)
  packet_size := 20 * 2 * 48000 / 1000
  for len(output_buffer) > packet_size {
    send_packet(output_buffer[:packet_size])
    output_buffer = output_buffer[packet_size:]
  }
  return 0
}

func send_packet(pcm []int16) {
  expected := 20 * 2 * 48000 / 1000
  if len(pcm) != expected {
    LOG.Println("ERROR: Expected a packet with", expected, "bytes, but got", len(pcm))
    return
  }
  opus := make([]byte, 1000)
  n, err := encoder.Encode(pcm, opus)
  if err != nil {
    LOG.Println("Error while encoding opus audio:", err)
    return
  }
  // Run this async, as OpusSend can block
  go func() {
    connected_voice.OpusSend <- opus[:n]
  }()
}

func add_pcm(speed float64, out_l, out_r []jack.AudioSample, pcm []int16) {
  for i := 0; i < len(out_l); i++ {
    // This is a sample position in pcm. (i) is the index into pcm,
    // and / speed will make sample_pos increase faster than i.
    sample_pos := float64(i) / speed
    index_float, weight := math.Modf(sample_pos)
    // This is the first sample in the pair that we need to average.
    // We average pcm[index*2] and pcm[(index+1)*2] later on.
    index := int(index_float)
    if (index + 1) * 2 > len(pcm) {
      // PCM is too short to do anything with
      return
    } else if (index + 2) * 2 > len(pcm) {
      // Means we have hit the end of the pcm data, so we should just grab the last sample
      // and then return.
      left  := float64(pcm[index * 2 + 0])
      right := float64(pcm[index * 2 + 1])
      out_l[i] += jack.AudioSample(left) / 32768 / jack.AudioSample(speed)
      out_r[i] += jack.AudioSample(right) / 32768 / jack.AudioSample(speed)
      return
    } else {
      // Weighted average between index and index+1
      left  := float64(pcm[index * 2 + 0]) * weight + float64(pcm[(index + 1) * 2 + 0]) * (1 - weight)
      right := float64(pcm[index * 2 + 1]) * weight + float64(pcm[(index + 1) * 2 + 1]) * (1 - weight)
      out_l[i] += jack.AudioSample(left) / 32768 / jack.AudioSample(speed)
      out_r[i] += jack.AudioSample(right) / 32768 / jack.AudioSample(speed)
      if out_l[i] > 1 || out_l[i] < -1 {
        LOG.Println(out_l[i])
      }
    }
  }
}

func init_voice(vc *discordgo.VoiceConnection) {
  vc.AddHandler(func(vc *discordgo.VoiceConnection, update *discordgo.VoiceSpeakingUpdate) {
    ssrc := uint32(update.SSRC)
    audio_stream_locks[ssrc] = &sync.Mutex{}
    audio_streams[ssrc] = &audio_buffer{
      buf: cbuf.New(),
      last_skip: 100,
      speed: 1,
      user_id: update.UserID,
    }
  })
  connected_voice = vc
  LOG.Println("JOINING VOICE")
  go func() {
    for {
      packet := <- connected_voice.OpusRecv
      ssrc := packet.SSRC
      _, ok := audio_streams[ssrc]
      if !ok {
        // We want to add to audio_streams within the voice update handler,
        // so that we can know who's talking.
        continue
      }
      // This audio data is an int16 for each sample,
      // alternating left and right.
      // 20ms frame size, 2 channels, 48000khz sample rate.
      // TODO: Remove hardcoded sample rate.
      pcm := make([]int16, 20 * 2 * 48000 / 1000)
      size, err := decoder.Decode(packet.Opus, pcm)
      if err != nil {
        LOG.Println("Error while decoding opus data:", err)
        continue
      }
      // Lock the list
      audio_stream_locks[ssrc].Lock()
      // * 2 because 2 channels
      audio_streams[ssrc].buf.PushInt16(pcm[:size * 2])
      audio_stream_locks[ssrc].Unlock()
    }
  }()
}
