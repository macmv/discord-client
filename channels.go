package main

import (
  "fmt"
  "sort"

  "github.com/bwmarrin/discordgo"

  "gitlab.com/macmv/discord-client/draw"
)

type channel_group struct {
  parent *discordgo.Channel
  children []*discordgo.Channel
}

// This sorts all of the channels in the guild into groups of
// categories.
func create_channel_list(list *draw.List, guild *discordgo.Guild) []*discordgo.Channel {
  // NOTE: channel.Position is relative to parents.
  // This means we need to first sort categories,
  // then sort everything else.
  categories := []channel_group{}
  // For any channels outside of a group.
  categories = append(categories, channel_group{})
  // List is out of order, so we must generate all categories first.
  for _, c := range guild.Channels {
    if c.Type == discordgo.ChannelTypeGuildCategory {
      categories = append(categories, channel_group{parent: c})
    }
  }
  // Used to look up index of parent
  category_map := make(map[string]int)
  for i, c := range categories {
    if i == 0 { continue }
    category_map[c.parent.ID] = i
  }
  // Add all the children into the []channel_group
  for _, c := range guild.Channels {
    if c.Type != discordgo.ChannelTypeGuildCategory {
      index := 0
      if c.ParentID != "" {
        // If the item is not in the map, index will be 0
        index, _ = category_map[c.ParentID]
      }
      categories[index].children = append(categories[index].children, c)
    }
  }
  // Sort the categories
  sort.Slice(categories, func(i, j int) bool {
    // If the parent is nil, then it is the empty category.
    // That means it should always be at the top of the list.
    if categories[i].parent == nil { return true }
    if categories[j].parent == nil { return false }
    return categories[i].parent.Position < categories[j].parent.Position
  })

  // Sort all of the children of each category
  for _, c := range categories {
    sort.Slice(c.children, func(i, j int) bool {
      return c.children[i].Position < c.children[j].Position
    })
  }
  list.Clear()
  for _, c := range categories {
    if c.parent != nil {
      list.Add("-- " + c.parent.Name)
    }
    for _, child := range c.children {
      list.Add(fmt.Sprint(child.Name))
      //switch child.Type {
      //case discordgo.ChannelTypeGuildText:
      //  list.Add(child.Name)
      //case discordgo.ChannelTypeDM:
      //  list.Add(child.Name)
      //case discordgo.ChannelTypeGuildVoice:
      //  list.Add(child.Name)
      //case discordgo.ChannelTypeGroupDM:
      //  list.Add(child.Name)
      //case discordgo.ChannelTypeGuildCategory:
      //  list.Add(child.Name)
      //case discordgo.ChannelTypeGuildNews:
      //  list.Add(child.Name)
      //case discordgo.ChannelTypeGuildStore:
      //  list.Add(child.Name)
      //}
    }
  }
  channels := []*discordgo.Channel{}
  for _, c := range categories {
    if c.parent != nil {
      channels = append(channels, c.parent)
    }
    for _, child := range c.children {
      channels = append(channels, child)
    }
  }
  return channels
}
