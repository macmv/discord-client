package discord

import (
  "fmt"
  "errors"
  "strings"
  "net/http"
  "encoding/json"
)

// This gets a token based on a username and password.
// Should only be used once, when the application first
// launches. After that, it should be loaded from disk.
func Login(email, pass string) (string, error) {
  url := "https://discord.com/api/v8/auth/login"
  payload := fmt.Sprintf(
    `{"login":"%v","password":"%v","undelete":false,"captcha_key":null,"login_source":null,"gift_code_sku_id":null}`,
    email,
    pass,
  )
  res, err := http.Post(url, "application/json", strings.NewReader(payload))
  if err != nil { return "", err }
  defer res.Body.Close()

  j := json.NewDecoder(res.Body)
  var data map[string]interface{}
  err = j.Decode(&data)
  if err != nil { return "", err }

  token, ok := data["token"]
  if ok {
    return token.(string), nil
  }
  return "", errors.New("Unable to fetch token")
}
