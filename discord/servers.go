package discord

import (
  "github.com/bwmarrin/discordgo"
)

type Discord struct {
  *discordgo.Session
}

func New(token string) (*Discord, error) {
  s, err := discordgo.New(token)
  if err != nil { return nil, err }

  err = s.Open()
  if err != nil { return nil, err }

  return &Discord{
    s,
  }, nil
}

