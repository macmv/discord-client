package draw

import (
  "github.com/rivo/tview"
  "github.com/gdamore/tcell/v2"
)

type list_item struct {
  text string
  style tcell.Style
  hovered_style tcell.Style
  selected_style tcell.Style
}

type List struct {
  *tview.Box

  items []list_item
  hovered int
  selected int
  wrap bool
  // The index at the top of the screen
  scroll int
  // See AutoScroll() for more info.
  auto_scroll bool

  selected_func func(index int, text string)
  back_func func()

  // Default style for list items
  style tcell.Style
  hovered_style tcell.Style
  selected_style tcell.Style
}

func NewList() *List {
  return &List{
    Box: tview.NewBox().
    SetBorder(true).
    SetBorderPadding(0, 0, 1, 1),
    items: []list_item{},
    wrap: true,
    style: tcell.StyleDefault.Foreground(tview.Styles.TitleColor),
    hovered_style: tcell.StyleDefault.Foreground(tview.Styles.PrimitiveBackgroundColor).Background(tview.Styles.TitleColor),
    selected_style: tcell.StyleDefault.Foreground(tview.Styles.InverseTextColor),
  }
}

func (l *List) Add(text string) {
  if l.auto_scroll {
    _, _, _, height := l.GetInnerRect()
    if l.scroll == len(l.items) - height {
      l.scroll++
    }
  }
  l.items = append(l.items, list_item{
    text: text,
    style: l.style,
    hovered_style: l.hovered_style,
    selected_style: l.selected_style,
  })
}

func (l *List) Clear() {
  l.items = nil
  l.hovered = 0
  l.selected = 0
}

func (l *List) Len() int {
  return len(l.items)
}

// If this is set, then when a new item is added to the list,
// so long as the list is already scrolled down, it will scroll
// the list all the way to the bottom. Used for a text messages
// type of thing.
func (l *List) AutoScroll(enabled bool) {
  l.auto_scroll = enabled
}

func (l *List) DefaultStyle() tcell.Style {
  return l.style
}

func (l *List) SetStyle(index int, style tcell.Style) {
  l.items[index].style = style
}

func (l *List) SetText(index int, text string) {
  l.items[index].text = text
}

func (l *List) SetSelectedFunc(f func(index int, text string)) {
  l.selected_func = f
}

func (l *List) SetBackFunc(f func()) {
  l.back_func = f
}

func (l *List) Selected() int {
  return l.selected
}

func (l *List) ItemText(index int) string {
  return l.items[index].text
}

func (l *List) Draw(screen tcell.Screen) {
  l.Box.DrawForSubclass(screen, l)

  x, y, w, h := l.GetInnerRect()
  right := x + w
  bottom := y + h
  total_width, total_height := screen.Size()
  if right > total_width {
    right = total_width
  }
  if bottom > total_height {
    bottom = total_height
  }

  for i := l.scroll; i < len(l.items); i++ {
    if y >= bottom {
      break
    }
    item := l.items[i]
    style := item.style
    if i == l.selected {
      style = item.selected_style
    }
    if i == l.hovered && l.HasFocus() {
      style = item.hovered_style
    }
    for j, c := range item.text {
      if x + j >= right {
        break
      }
      screen.SetContent(x + j, y, c, nil, style)
    }
    y++
  }
}

func (l *List) InputHandler() func(event *tcell.EventKey, set_focus func(p tview.Primitive)) {
  return l.WrapInputHandler(func(event *tcell.EventKey, set_focus func(p tview.Primitive)) {
    if len(l.items) == 0 {
      return
    }

    switch key := event.Key(); key {
    case tcell.KeyEscape:
      if l.back_func != nil {
        l.back_func()
      }
    case tcell.KeyTab, tcell.KeyDown, tcell.KeyRight:
      l.hovered++
    case tcell.KeyBacktab, tcell.KeyUp, tcell.KeyLeft:
      l.hovered--
    case tcell.KeyHome:
      l.hovered = 0
    case tcell.KeyEnd:
      l.hovered = len(l.items) - 1
    case tcell.KeyPgDn:
      _, _, _, height := l.GetInnerRect()
      l.hovered += height
    case tcell.KeyPgUp:
      _, _, _, height := l.GetInnerRect()
      l.hovered -= height
    case tcell.KeyEnter:
      item := l.items[l.hovered]
      if l.selected_func != nil {
        l.selected_func(l.hovered, item.text)
      }
      // Make sure to set this after selected_func(),
      // so that Selected() will give the previous item.
      l.selected = l.hovered
    case tcell.KeyRune:
      switch event.Rune() {
      case 'j':
        l.hovered++
      case 'k':
        l.hovered--
      case 'g':
        if event.Modifiers() & tcell.ModShift != 0 {
          // Means shift-g was pressed
          l.hovered = len(l.items) - 1
        } else {
          // Means g was pressed
          l.hovered = 0
        }
      case 'l':
        item := l.items[l.hovered]
        if l.selected_func != nil {
          l.selected_func(l.hovered, item.text)
        }
        // Make sure to set this after selected_func(),
        // so that Selected() will give the previous item.
        l.selected = l.hovered
      case 'h':
        if l.back_func != nil {
          l.back_func()
        }
      }
    }

    if l.hovered < 0 {
      if l.wrap {
        l.hovered = len(l.items) - 1
      } else {
        l.hovered = 0
      }
    } else if l.hovered >= len(l.items) {
      if l.wrap {
        l.hovered = 0
      } else {
        l.hovered = len(l.items) - 1
      }
    }
    _, _, _, height := l.GetInnerRect()
    if l.hovered < l.scroll {
      // Makes sure the list is scrolled upwards
      l.scroll = l.hovered
    } else if l.hovered >= l.scroll + height {
      // Makes sure the list is scrolled downwards
      l.scroll = l.hovered - height + 1
    }
  })
}
