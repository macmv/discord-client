package draw

import (
  "github.com/rivo/tview"
  "github.com/gdamore/tcell/v2"
)

type TextInput struct {
  *tview.Box

  text string
  cursor int

  // The style of the text
  style tcell.Style

  send_func func()
  back_func func()
}

func NewTextInput() *TextInput {
  return &TextInput{
    Box: tview.NewBox().
    SetBorder(true).
    SetBorderPadding(0, 0, 1, 1),
  }
}

func (t *TextInput) Text() string {
  return t.text
}

func (t *TextInput) SetSendFunc(f func()) {
  t.send_func = f
}

func (t *TextInput) SetBackFunc(f func()) {
  t.back_func = f
}

func (t *TextInput) Draw(screen tcell.Screen) {
  t.Box.DrawForSubclass(screen, t)

  x, y, w, _ := t.GetInnerRect()
  right := x + w
  total_width, _ := screen.Size()
  if right > total_width {
    right = total_width
  }

  for j, c := range t.text {
    if x + j >= right {
      break
    }
    screen.SetContent(x + j, y, c, nil, t.style)
  }

  if t.HasFocus() {
    screen.ShowCursor(x + t.cursor, y)
  }
}

func (t *TextInput) InputHandler() func(event *tcell.EventKey, set_focus func(p tview.Primitive)) {
  return t.WrapInputHandler(func(event *tcell.EventKey, set_focus func(p tview.Primitive)) {
    switch event.Key() {
    case tcell.KeyEscape:
      if t.back_func != nil {
        t.back_func()
      }
    case tcell.KeyEnter:
      if t.send_func != nil {
        t.send_func()
        t.text = ""
        t.cursor = 0
      }
    case tcell.KeyLeft:
      if t.cursor > 0 {
        t.cursor--
      }
    case tcell.KeyRight:
      if t.cursor < len(t.text) {
        t.cursor++
      }
    case tcell.KeyDelete:
      if t.cursor == len(t.text) {
        break
      }
      t.text = t.text[:t.cursor] + t.text[t.cursor+1:]
    case tcell.KeyBackspace, tcell.KeyBackspace2:
      if t.cursor == 0 {
        break
      }
      t.text = t.text[:t.cursor-1] + t.text[t.cursor:]
      t.cursor--
    case tcell.KeyRune:
      t.text += string(event.Rune())
      t.cursor++
    }
  })
}
