module gitlab.com/macmv/discord-client

go 1.15

require (
	github.com/bwmarrin/discordgo v0.23.1
	github.com/gdamore/tcell/v2 v2.0.1-0.20201017141208-acf90d56d591
	github.com/rivo/tview v0.0.0-20210125085121-dbc1f32bb1d0
	github.com/xthexder/go-jack v0.0.0-20201026211055-5b07fb071116
	gitlab.com/macmv/cbuf v0.0.0-20210218200835-69cf0c2e380d
	gopkg.in/hraban/opus.v2 v2.0.0-20201025103112-d779bb1cc5a2
)
