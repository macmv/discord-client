package main

import (
  "os"
  "log"
  "fmt"
  "bufio"
  "strings"
  "io/ioutil"

  "github.com/rivo/tview"

  "gitlab.com/macmv/discord-client/draw"
  "gitlab.com/macmv/discord-client/discord"
)

var LOG *log.Logger

func main() {
  f, err := os.OpenFile(".cache/log.txt", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
  if err != nil {
    log.Fatal(err)
  }
  LOG = log.New(f, "", log.LstdFlags)

  t, err := ioutil.ReadFile(".cache/token.txt")
  token := string(t)
  if err != nil {
    token, err = login()
    if err != nil { panic(err) }
  }

  d, err := discord.New(strings.TrimSpace(token))
  if err != nil { panic(err) }

  LOG.Println("Logged in!")

  a := tview.NewApplication()
  a.EnableMouse(true)

  //friends := tview.NewList()
  //for _, r := range d.State.Relationships {
  //  if r.Type == 1 {
  //    friends.AddItem(r.User.Username, "", 'a', nil)
  //  }
  //}

  server_list := draw.NewList()
  channel_pages := tview.NewPages()
  channel_lists := []*draw.List{}

  for _, g := range d.State.Guilds {
    server_list.Add(g.Name)

    // This is the list of channels
    list := draw.NewList()
    channel_lists = append(channel_lists, list)
    // This is a sorted []*discordgo.Channel
    channels := create_channel_list(list, g)
    // This is a struct with tview.Pages,
    // containing all of the lists needed by every text channel.
    messages := create_messages(a, d.Session, g, channels)

    list.SetSelectedFunc(func (index int, title string) {
      messages.Select(channels[index].ID)
    })
    list.SetBackFunc(func () {
      a.SetFocus(server_list)
    })

    flex := tview.NewFlex()
    flex.AddItem(list, 0, 1, true)
    flex.AddItem(messages, 0, 6, false)

    channel_pages.AddPage(g.ID, flex, true, false)
  }
  server_list.SetSelectedFunc(func (index int, title string) {
    channel_pages.SwitchToPage(d.State.Guilds[index].ID)
    a.SetFocus(channel_pages)
  })

  root := tview.NewFlex()
  root.AddItem(server_list, 0, 1, true)
  root.AddItem(channel_pages, 0, 7, false)

  a.SetRoot(root, true)

  err = a.Run()
  if err != nil {
    panic(err)
  }
}

func login() (string, error) {
  r := bufio.NewReader(os.Stdin)

  fmt.Print("Email: ")
  email, _ := r.ReadString('\n')
  email = strings.TrimSpace(email)

  fmt.Print("Password: ")
  pass, _ := r.ReadString('\n')
  pass = strings.TrimSpace(pass)

  token, err := discord.Login(email, pass)
  if err != nil { return "", err }

  err = os.MkdirAll(".cache", 0755)
  if err != nil { return "", err }

  f, err := os.Create(".cache/token.txt")
  if err != nil { return "", err }
  defer f.Close()

  err = os.Chmod(".cache/token.txt", 0600)
  if err != nil { return "", err }

  f.Write([]byte(token))

  return token, nil
}
