package main

import (
  "github.com/rivo/tview"
  "github.com/gdamore/tcell/v2"

  "github.com/bwmarrin/discordgo"

  "gitlab.com/macmv/discord-client/draw"
)

type channel_and_list struct {
  channel *discordgo.Channel
  list *draw.List
}

type messages struct {
  *tview.Flex
  pages *tview.Pages
  message_input *draw.TextInput
  app *tview.Application

  sess *discordgo.Session
  guild *discordgo.Guild
  current_channel string
  // List of channel ids, used to access channel_map in order.
  channels []string
  channel_map map[string]channel_and_list
}

func create_messages(app *tview.Application, sess *discordgo.Session, guild *discordgo.Guild, channels []*discordgo.Channel) *messages {
  pages := tview.NewPages()
  channel_list := []string{}
  channel_map := map[string]channel_and_list{}
  for _, c := range channels {
    cl := channel_and_list{channel: c}
    channel_list = append(channel_list, c.ID)

    if c.Type == discordgo.ChannelTypeGuildText {
      list := draw.NewList()
      list.AutoScroll(true)
      list.Add(c.Name)
      for _, m := range c.Messages {
        list.Add(m.Content)
      }
      cl.list = list
      pages.AddPage(
        c.ID,
        list,
        true,
        false,
      )
    }
    channel_map[c.ID] = cl
  }
  m := &messages{
    pages: pages,
    app: app,
    sess: sess,
    guild: guild,
    channels: channel_list,
    channel_map: channel_map,
  }
  m.message_input = draw.NewTextInput()
  m.message_input.SetSendFunc(func() {
    text := m.message_input.Text()
    list := m.channel_map[m.current_channel].list
    list.Add(text)
    // Make the message gray at first (to show it hasn't been sent).
    list.SetStyle(list.Len() - 1, list.DefaultStyle().Foreground(tcell.NewHexColor(0x888888)))

    go func(text string, list *draw.List, index int) {
      // Send async
      sess.ChannelMessageSend(m.current_channel, text)
      // Sync the update to the list
      app.QueueUpdateDraw(func() {
        // Make the message no longer grayed out, to confirm that it has been sent.
        list.SetStyle(index, list.DefaultStyle())
      })
    }(text, list, list.Len() - 1)
  })
  sess.AddHandler(func(s *discordgo.Session, message *discordgo.MessageCreate) {
    c, ok := m.channel_map[message.ChannelID]
    if ok {
      c.list.Add(message.Content)
    }
    // This is safe to call in another goroutine.
    app.Draw()
  })

  m.Flex = tview.NewFlex()
  m.Flex.SetDirection(tview.FlexRow)
  m.Flex.AddItem(pages, 0, 1, false)
  m.Flex.AddItem(m.message_input, 3, 1, true)

  return m
}

// This loads messages from a cache or api request.
func (m *messages) Select(id string) {
  cl, ok := m.channel_map[id]
  if !ok { return }
  channel := cl.channel
  switch channel.Type {
  case discordgo.ChannelTypeGuildText:
    list := cl.list
    if list.Len() != 0 {
      messages, err := m.sess.ChannelMessages(channel.ID, 10, "", "", "")
      list.Add(channel.Name)
      if err != nil { list.Add(err.Error()) }
      for i := len(messages) - 1; i >= 0; i-- {
        list.Add(messages[i].Content)
      }
    }
    m.pages.SwitchToPage(id)
    m.current_channel = id
    m.app.SetFocus(m.message_input)
  case discordgo.ChannelTypeGuildVoice:
    vc, err := m.sess.ChannelVoiceJoin(m.guild.ID, id, false, false)
    if err != nil {
      LOG.Println(err)
      break
    }
    init_voice(vc)
  }
}
